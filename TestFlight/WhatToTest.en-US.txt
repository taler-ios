Version 0.14.4 (wallet-core 0.14.9)

- bugfixes
- scanning a QR code works again


Version 0.14.3 (wallet-core 0.14.5)

• New feature: OIM when rotating the phone
- improved error handling
- lots of bugfixes


Version 0.14.2 (wallet-core 0.14.4)

• New feature: cash-to-ecash with a Cash Acceptor device
- Polish localization


Version 0.14.1 (wallet-core 0.14.1)

- bugfix: withdraw-template works again
- Spanish localization


Version 0.14.0 (wallet-core 0.14.0)

• New UI with Action button
- lots of bugfixes
- German localization


Version 0.13.3 (wallet-core 0.13.5)

• Swipe left to delete transactions
- bugfix for deleting payment providers


Version 0.13.2 (wallet-core 0.13.4)

- bugfix for deposit


Version 0.13.1 (wallet-core 0.13.3)

• Repurchase
• Payto-sharing adds human readable text


Version 0.13.0 (wallet-core 0.13.0)

• KYC for balances - use bank.kyctest.taler.net to withdraw
• Exchanges can be deleted


Version 0.12.1 (wallet-core 0.12.12)

• Ukrainian localization - Slava Ukraini!


Version 0.12.0 (wallet-core 0.12.9)

• New icons for actions
• New withdrawal workflow - enter amount in the wallet app
• show how long exchange takes to wire back funds for aborted withdrawals
- Bugfixes
- lots of layout improvements
- Fee computation works again
- Amounts should no longer mix even if exchanges have the same currency name


Version 0.11.2 (wallet-core 0.11.4)

- Bugfix: Send P2P had the comparison (amount vs available) wrong


Version 0.11.1 (wallet-core 0.11.3)

- Bugfixes


Version 0.11.0 (wallet-core 0.11.1)

• Spanish localization
- Pay-Templates overhauled


Version 0.10.3 (wallet-core 0.10.8)

- Error handling
- Bugfixes


Version 0.10.2 (wallet-core 0.10.7)

- Improve German localization
- Minor bugfixes


Version 0.10.1 (wallet-core 0.10.7)

• German localization
- Improve observability and error handling


Version 0.10.0 (wallet-core 0.10.6)

• Use segmented control instead of drop-down picker for bank selection in manual withdrawal
• Observability via LocalConsole
- accessibility improvements


Version 0.9.7 (wallet-core 0.10.6)

• better error handling
- bugfix: wallet-core cache handling & fee computations


Version 0.9.6 (1)

• New feature: Removed 'Banking' tab. Withdraw + deposit are now directly available in Balances
    Added Settings->Payment Services for exchange management (w.i.p.)
• New feature: LocalConsole also for Taler Wallet
- bugfix: (pending) refresh transactions are now shown correctly
- bugfix: wallet-core 0.10.2 speeds up fee computations


Version 0.9.5 (3)

- bugfix: wallet-core 0.10.1 fixes exchange handling


Version 0.9.5 (2)

• Added privacy manifest


Version 0.9.5 (1)

• New feature: taler://withdraw-exchange no longer requires the pubkey of the exchange
- bugfix: manual withdrawal now handles x-taler-bank correctly, and shows payee
- bugfix: expired denominations in withdrawals are retried with new denominations


Version 0.9.4 (12)

• Direct withdrawal of 25 KUDOS when the wallet is empty, no need to register a bank account first


Version 0.9.4 (11)

• New views for empty wallet (Balances) and zero payment providers (Banking)


Version 0.9.4 (10)

• New feature: Native Networking instead of curl


Version 0.9.4 (9)

• debug version WITHOUT c-ares


Version 0.9.4 (8)

• New feature: LocalConsole for GNU Taler only. Find it in Settings...

also GNU Taler no longer claims the taler:// URL scheme. You can now install GNU Taler for debugging/testing in parallel to Taler Wallet, and all System events (System Camera, Mail Messages, NFC Tags, ...) will always go to Taler Wallet.
Thus you need to scan a QR code from another device (laptop, tablet, phone, ...) to withdraw into GNU Taler, and cannot use the browser on this iPhone for bank-integrated withdrawals, because attempting to open a taler://withdraw/ will open Taler Wallet and not GNU Taler.


Version 0.9.4 (7)

• New feature: Pay-Templates


Version 0.9.4 (6)

• New task scheduler in wallet-core
- Accessibility improvements


Version 0.9.4 (5)

• New feature: Deposit
• New feature: Multiple Exchanges with the same currency
- tapping on the selected tab item again pops to root
- rewards / surveys no longer supported - use P2P instead


Version 0.9.4 (4)

- For non-global currencies the exchange is shown
- Accessibility improvements


Version 0.9.4 (3)

- Payment shows the fulfillment link
- P2P directly dismisses the sheet on success
- the git version hash now only shows the first 7 letters


Version 0.9.4 (2)

• New feature: curl with c-ares improves networking
- bug fix: currency names default to the long version if a currency symbol is not available
- bug fix: version info in About dialog is now also showing the build number (in parentheses)

A/B Test: Taler Wallet uses c-ares, GNU Taler doesn't. You can install both apps to test this feature, but then you cannot use bank-integrated withdrawals for both apps with Safari running the bank website on the same iPhone anymore since iOS doesn't let you choose which wallet app to withdraw to. Instead you can open the bank website https://bank.demo.taler.net on your computer or tablet, start the withdrawal there, and scan the QR code with the app (Taler Wallet or GNU Taler) you want to receive the money in.


Version 0.9.4 (1)

- now accepts uppercase TALER://PAY URIs


Version 0.9.4 (0)

- bug fixes
- first release of 0.9.4


Version 0.9.3 (34)

• New feature: Demo Bank Website button in Banking
- Balances is shown after a transaction finished


Version 0.9.3 (33)

• New feature: ToS with Markdown
- background colors adapted for WCAG AA


Version 0.9.3 (32)

• New feature: Currency Conversion
• New feature: ToS with multiple languages
• New feature: Landscape
• New feature: Warnings for Delete, Fail & Abort

- Bugfix: Manual withdrawals no longer create multiple identical transactions
- "Banking" instead of "Exchanges" - withdraw and deposit
- Haptic feedback for Copy/Share buttons


Version 0.9.3 (31)

• New feature: Shortcuts in P2P directly advance to subject
• New feature: ToS in P2P receive

- improved withdrawal UI


Version 0.9.3 (30)

- Bugfix: Manual withdrawal works again (was broken in 29)
- Bugfix: Currency input with zero fractional digits (test.taler.net)


Version 0.9.3 (29)

• New feature: ToS in Exchanges Tab
- Bugfix: P2P Send works again (was broken in 28)


Version 0.9.3 (28)

• Unsuccessful (Incomplete) transactions no longer have their own list, but are now
  included in the main transaction list.
  But they still show the original amount instead of the real cost (fees) only. tbc


Version 0.9.3 (27)

• New feature: Badges for KYC and Confirm with bank (for bank-integrated withdrawals)
    start a withdrawal from the bank website, but then don't "Confirm with bank" immediately but press "Confirm later"
    request some money, tap on Done without sharing or scanning the talerURI QR.
    Notice the difference in the Pending list
• KYC in pending transactions accessible from details

- Bugfix: after P2P no QR code was shown
- Layout improvements iOS16+. We'll continue to support iOS15, it just won't look so nice as on iOS 16+.


Version 0.9.3 (26)

• New feature: KYC Support. But you won't get a second chance if you don't use the first one.
    (KYC will be added to pending transactions in the next version)

- Bugfix: With more than one exchange currencies were mixed in P2P
- The whole textfield can now be used to bring up the keyboard in P2P


Version 0.9.3 (25)

• New feature: Shortcuts Buttons (50,25,10,5) for P2P + Withdrawal

- CurrencyFormatter for P2P + Withdrawal
- Bugfix: Accessibility announcements always did reset the focus to start of view

A/B test result: Nobody liked GNU Taler (with SideView & Hamburger-button) better,
thus we concentrate on Taler Wallet (with TabBar)


Version 0.9.3 (24)

• Sound & Haptics ON by default
• Blue Taler Logo
- Lots of Accessibility improvements
- Bugfix: P2P Transactions were started twice
- Incomplete Transactions no longer show "Obtained" or "Paid"


Version 0.9.3 (23)

- Fixed layout for Pending + Transactions for dynamic font sizes
- CurrencyFormatter for Pending + Transactions


Version 0.9.3 (22)

- Fixed layout for Balances for dynamic font sizes


Version 0.9.3 (21)

- Bugfix: P2P payments should work again
- CurrencyFormatter for Balance


Version 0.9.3 (20)

• A/B test: GNU Taler with hamburger button and sideview, Taler Wallet with tab bar
• New feature: bar graphic after currency header name representing recent transactions

- Usability improvements
  Button layout in Balances and Exchanges
- Bugfix: Textsize is changeable again
  (but font is not changeable in this version)


Version 0.9.3 (19)

• New feature: database is now sqlite3

- Usability improvements


Version 0.9.3 (18)

- Should work now with https://kaufen.tschunk.shop


Version 0.9.3 (17)

• New Feature: Payment Sounds from carlo von lynX

- Usability improved for manual withdrawal + P2P


Version 0.9.3 (16)

• Switch on Developer Mode in Settings, then
• Run Integration Test (Demo 1), then immediately switch back to Balances
• (repeat with Demo 2)

- Terms of Service more self-explaining (check this before running the integration test)
- Expiration pre-selected (middle button) in P2P
- Transaction details: Status field on top for better recognition
- Balances shows the last 3 transactions
- QR screen no longer vanishes immediately (P2P)


Version 0.9.3 (15)

• New Feature: Reset wallet (Throw away all your coins)
• Insufficient Balance detected when trying to pay sth.

- Integration Test works again (was broken in 10..14)


Version 0.9.3 (14)

- ToS formatted with markdown (if the exchange supports this)
- Payment in the shop works again (was broken in 12 + 13)


Version 0.9.3 (13)

- Checks that the amount is valid when sending coins
- Shows the P2P purpose in the transaction details


Version 0.9.3 (12)

• Try to add an exchange, and then withdraw money from that

- Automatic reload of balances


Version 0.9.3 (11)

- Bank-integrated withdrawal now hints better that you should confirm with bank


Version 0.9.3 (10)

• Try to withdraw some KUDOS (by tapping the link in Balances)
• Try to spend them at the store
• Try to send a friend/colleague some money
