/*
 * This file is part of GNU Taler, ©2022-25 Taler Systems S.A.
 * See LICENSE.md
 */
/**
 * Model+Exchange
 *
 * @author Marc Stibane
 */
import Foundation
import taler_swift
import SymLog
fileprivate let ASYNCDELAY: UInt = 0   //set e.g to 6 or 9 seconds for debugging

struct ExchangeError: Codable, Hashable {
    var error: TalerErrorDetail
}

enum ExchangeEntryStatus: String, Codable {
    case preset
    case ephemeral
    case used
}

enum ExchangeUpdateStatus: String, Codable {
    case initial
    case initialUpdate = "initial-update"
    case suspended
    case failed
    case outdatedUpdate = "outdated-update"
    case ready
    case readyUpdate = "ready-update"
    case unavailableUpdate = "unavailable-update"
}

struct ExchangeState: Codable, Hashable {
    var exchangeEntryStatus: ExchangeEntryStatus
    var exchangeUpdateStatus: ExchangeUpdateStatus
    var tosStatus: ExchangeTosStatus
}

struct ExchangeTransition: Codable {             // Notification
    enum TransitionType: String, Codable {
        case transition = "exchange-state-transition"
    }
    var type: TransitionType
    var exchangeBaseUrl: String
    var oldExchangeState: ExchangeState
    var newExchangeState: ExchangeState
}
// MARK: -
/// The result from wallet-core's ListExchanges
struct Exchange: Codable, Hashable, Identifiable {
    static func < (lhs: Exchange, rhs: Exchange) -> Bool {
        let leftScope = lhs.scopeInfo
        let rightScope = rhs.scopeInfo
        return leftScope < rightScope
    }
    static func == (lhs: Exchange, rhs: Exchange) -> Bool {
        return lhs.exchangeBaseUrl == rhs.exchangeBaseUrl
        &&     lhs.tosStatus == rhs.tosStatus
//        &&     lhs.exchangeStatus == rhs.exchangeStatus                         // deprecated
        &&     lhs.exchangeEntryStatus == rhs.exchangeEntryStatus
        &&     lhs.exchangeUpdateStatus == rhs.exchangeUpdateStatus
    }

    var exchangeBaseUrl: String
    var masterPub: String?
    var scopeInfo: ScopeInfo
    var paytoUris: [String]
    var tosStatus: ExchangeTosStatus
    var exchangeEntryStatus: ExchangeEntryStatus
    var exchangeUpdateStatus: ExchangeUpdateStatus
    var peerPaymentsDisabled: Bool?
    var noFees: Bool?
    var ageRestrictionOptions: [Int]
    var lastUpdateTimestamp: Timestamp?
    var lastUpdateErrorInfo: ExchangeError?

    var id: String { exchangeBaseUrl + tosStatus.rawValue }
    var name: String? {
        if let url = URL(string: exchangeBaseUrl) {
            if let host = url.host {
                return host
            }
        }
        return nil
    }
}
// MARK: -
/// A request to list exchanges names for a currency
fileprivate struct ListExchanges: WalletBackendFormattedRequest {
    func operation() -> String { "listExchanges" }
    func args() -> Args { Args(filterByScope: scope, filterByExchangeEntryStatus: filterByStatus) }

    var scope: ScopeInfo?
    var filterByStatus: ExchangeEntryStatus?
    struct Args: Encodable {
        var filterByScope: ScopeInfo?
        var filterByExchangeEntryStatus: ExchangeEntryStatus?
    }
    struct Response: Decodable {        // list of known exchanges
        var exchanges: [Exchange]
    }
}

/// A request to get info for one exchange.
fileprivate struct GetExchangeByUrl: WalletBackendFormattedRequest {
    func operation() -> String { "getExchangeEntryByUrl" }
    func args() -> Args { Args(exchangeBaseUrl: exchangeBaseUrl) }

    var exchangeBaseUrl: String

    struct Args: Encodable {
        var exchangeBaseUrl: String
    }
    typealias Response = Exchange
}

/// A request to update a single exchange.
fileprivate struct UpdateExchange: WalletBackendFormattedRequest {
    func operation() -> String { "updateExchangeEntry" }
//    func args() -> Args { Args(scopeInfo: scopeInfo) }
    func args() -> Args { Args(exchangeBaseUrl: exchangeBaseUrl) }

//    var scopeInfo: ScopeInfo
    var exchangeBaseUrl: String
    struct Args: Encodable {
        var exchangeBaseUrl: String
    }
    struct Response: Decodable {}   // no result - getting no error back means success
}

/// A request to add an exchange.
fileprivate struct AddExchange: WalletBackendFormattedRequest {
    func operation() -> String { "addExchange" }     // addExchangeEntry
    func args() -> Args { Args(exchangeBaseUrl: exchangeBaseUrl) }

    var exchangeBaseUrl: String
    struct Args: Encodable {
        var exchangeBaseUrl: String
    }
    struct Response: Decodable {}   // no result - getting no error back means success
}

/// A request to delete an exchange.
fileprivate struct DeleteExchange: WalletBackendFormattedRequest {
    func operation() -> String { "deleteExchange" }
    func args() -> Args { Args(exchangeBaseUrl: exchangeBaseUrl, purge: purge) }

    var exchangeBaseUrl: String
    var purge: Bool
    struct Args: Encodable {
        var exchangeBaseUrl: String
        var purge: Bool
    }
    struct Response: Decodable {}   // no result - getting no error back means success
}

/// A request to get info about a currency
fileprivate struct GetCurrencySpecification: WalletBackendFormattedRequest {
    func operation() -> String { "getCurrencySpecification" }
    func args() -> Args { Args(scope: scope) }

    var scope: ScopeInfo
    struct Args: Encodable {
        var scope: ScopeInfo
    }
    struct Response: Codable, Sendable {
        let currencySpecification: CurrencySpecification
    }
}
/// A request to make a currency "global"
fileprivate struct AddGlobalCurrency: WalletBackendFormattedRequest {
    func operation() -> String { "addGlobalCurrencyExchange" }
    func args() -> Args { Args(currency: currency,
                        exchangeBaseUrl: baseUrl,
                      exchangeMasterPub: masterPub) }
    var currency: String
    var baseUrl: String
    var masterPub: String
    struct Args: Encodable {
        var currency: String
        var exchangeBaseUrl: String
        var exchangeMasterPub: String
    }
    struct Response: Decodable {}   // no result - getting no error back means success
}
fileprivate struct RmvGlobalCurrency: WalletBackendFormattedRequest {
    func operation() -> String { "removeGlobalCurrencyExchange" }
    func args() -> Args { Args(currency: currency,
                        exchangeBaseUrl: baseUrl,
                      exchangeMasterPub: masterPub) }
    var currency: String
    var baseUrl: String
    var masterPub: String
    struct Args: Encodable {
        var currency: String
        var exchangeBaseUrl: String
        var exchangeMasterPub: String
    }
    struct Response: Decodable {}   // no result - getting no error back means success
}
// MARK: -
extension WalletModel {
    /// ask wallet-core for its list of known exchanges
    nonisolated func listExchanges(scope: ScopeInfo?, filterByStatus: ExchangeEntryStatus? = nil, viewHandles: Bool = false)
      async -> [Exchange] {   // M for MainActor
        do {
            let request = ListExchanges(scope: scope, filterByStatus: filterByStatus)    // .used, .preset
            let response = try await sendRequest(request, ASYNCDELAY, viewHandles: viewHandles)
            return response.exchanges
        } catch {
            return []               // empty, but not nil
        }
    }

    /// add a new exchange with URL to the wallet's list of known exchanges
    nonisolated func getExchangeByUrl(url: String, viewHandles: Bool = false)
      async throws -> Exchange {
        let request = GetExchangeByUrl(exchangeBaseUrl: url)
//            logger.info("query for exchange: \(url, privacy: .public)")
        let response = try await sendRequest(request, viewHandles: viewHandles)
        return response
    }

    /// add a new exchange with URL to the wallet's list of known exchanges
    nonisolated func addExchange(url: String, viewHandles: Bool = false)
      async throws {
        let request = AddExchange(exchangeBaseUrl: url)
        logger.info("adding exchange: \(url, privacy: .public)")
        _ = try await sendRequest(request, viewHandles: viewHandles)
    }

    /// add a new exchange with URL to the wallet's list of known exchanges
    nonisolated func deleteExchange(url: String, purge: Bool = false, viewHandles: Bool = false)
      async throws {
        let request = DeleteExchange(exchangeBaseUrl: url, purge: purge)
        logger.info("deleting exchange: \(url, privacy: .public)")
        _ = try await sendRequest(request, viewHandles: viewHandles)
    }

    /// ask wallet-core to update an existing exchange by querying it for denominations, fees, and scoped currency info
//    func updateExchange(scopeInfo: ScopeInfo)
//    func updateExchange(scopeInfo: ScopeInfo, viewHandles: Bool = false)
    nonisolated func updateExchange4711(exchangeBaseUrl: String, viewHandles: Bool = false)
      async throws  {
//        let request = UpdateExchange(scopeInfo: scopeInfo)
        let request = UpdateExchange(exchangeBaseUrl: exchangeBaseUrl)
//        logger.info("updating exchange for: \(scopeInfo.currency, privacy: .public)")
        logger.info("updating exchange: \(exchangeBaseUrl, privacy: .public)")
        _ = try await sendRequest(request, viewHandles: viewHandles)
    }

    nonisolated func getCurrencyInfo(scope: ScopeInfo, delay: UInt = 0, viewHandles: Bool = false)
      async throws -> CurrencyInfo {
        let request = GetCurrencySpecification(scope: scope)
        let response = try await sendRequest(request, ASYNCDELAY + delay, viewHandles: viewHandles)
        return CurrencyInfo(specs: response.currencySpecification,
                        formatter: CurrencyFormatter.formatter(currency: scope.currency,
                                                                  specs: response.currencySpecification))
    }

    nonisolated func addGlobalCurrencyExchange(currency: String, baseUrl: String, masterPub: String, viewHandles: Bool = false)
      async throws {
        let request = AddGlobalCurrency(currency: currency, baseUrl: baseUrl, masterPub: masterPub)
        _ = try await sendRequest(request, viewHandles: viewHandles)
    }

    nonisolated func rmvGlobalCurrencyExchange(currency: String, baseUrl: String, masterPub: String, viewHandles: Bool = false)
      async throws {
        let request = RmvGlobalCurrency(currency: currency, baseUrl: baseUrl, masterPub: masterPub)
        _ = try await sendRequest(request, viewHandles: viewHandles)
    }
}
