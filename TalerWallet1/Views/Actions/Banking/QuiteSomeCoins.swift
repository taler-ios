/*
 * This file is part of GNU Taler, ©2022-25 Taler Systems S.A.
 * See LICENSE.md
 */
/**
 * @author Marc Stibane
 */
import SwiftUI
import taler_swift
import SymLog

struct CoinData {
    let numCoins: Int       // 0 == invalid, -1 == unknown
    var unknown:   Bool { numCoins < 0 }
    var invalid:   Bool { numCoins == 0 }
    var manyCoins: Bool { numCoins > 99 }
    var quiteSome: Bool { numCoins > 199 }
    var tooMany:   Bool { numCoins > 999 }

    let fee: Amount?
    func feeLabel(_ scope: ScopeInfo?, feeZero: String?, isNegative: Bool = false) -> (String, String) {
        if let fee {
            let formatted = fee.formatted(scope, isNegative: false)
            let feeLabel = fee.isZero ? feeZero ?? EMPTYSTRING    // String(localized: "No withdrawal fee")
                         : isNegative ? String(localized: "- \(formatted.0) fee")
                                      : String(localized: "+ \(formatted.0) fee")
            let feeA11Y = fee.isZero ? feeZero ?? EMPTYSTRING    // String(localized: "No withdrawal fee")
                        : isNegative ? String(localized: "- \(formatted.1) fee")
                                     : String(localized: "+ \(formatted.1) fee")
            return (feeLabel, feeA11Y)
       } else {
           return (EMPTYSTRING, EMPTYSTRING)
       }
    }
}

extension CoinData {
    init(coins numCoins: Int?, fee: Amount?) {
        self.init(numCoins: numCoins ?? -1, fee: fee)           // either the number of coins, or unknown
    }

    init(details: WithdrawalDetailsForAmount?) {
        do {
            if let details {
                // Incoming: fee = raw - effective
                let aFee = try details.amountRaw - details.amountEffective
                self.init(numCoins: details.numCoins ?? -1,    // either the number of coins, or unknown
                               fee: aFee)
                return
            }
        } catch {}
        self.init(numCoins: 0, fee: nil)      // invalid
    }

    init(details: CheckPeerPullCreditResponse?) {
        do {
            if let details {
                // Incoming: fee = raw - effective
                let aFee = try details.amountRaw - details.amountEffective
                self.init(numCoins: details.numCoins ?? -1,    // either the number of coins, or unknown
                               fee: aFee)
                return
            }
        } catch {}
        self.init(numCoins: 0, fee: nil)      // invalid
    }
}
// MARK: -
struct QuiteSomeCoins: View {
    private let symLog = SymLogV(0)
    let scope: ScopeInfo?
    let coinData: CoinData
    let shouldShowFee: Bool
    let feeIsNegative: Bool

    var body: some View {
        let isError = coinData.tooMany // || coinData.invalid
        let showFee = shouldShowFee && !isError
        if showFee {    // TODO: does exchange never have fee?
            if let fee = coinData.fee {
                let feeLabel = coinData.feeLabel(scope,
                                        feeZero: String(localized: "No fee"),
                                     isNegative: feeIsNegative)
                Text(feeLabel.0)
                    .accessibilityLabel(feeLabel.1)
                    .foregroundColor(.primary)
                    .talerFont(.body)
            }
        }
        if isError || coinData.quiteSome || coinData.manyCoins {
            Text(coinData.tooMany ? "Amount too big for a single withdrawal!"
             : coinData.quiteSome ? "Note: It will take quite some time to prepare this amount! Be more patient..."
                                  : "Note: It will take some time to prepare this amount. Be patient...")
                .foregroundColor(isError || coinData.quiteSome ? WalletColors().attention : .primary)
                .talerFont(.body)
                .multilineTextAlignment(.leading)
                .padding(.vertical, 6)
        }
    }
}
// MARK: -
//struct QuiteSomeCoins_Previews: PreviewProvider {
//    static var previews: some View {
//        QuiteSomeCoins(coinData: CoinData(numCoins: 4, fee: Amount(currency: LONGCURRENCY, cent: 20) ),
//                  shouldShowFee: true,
//                amountEffective: nil)
//    }
//}
