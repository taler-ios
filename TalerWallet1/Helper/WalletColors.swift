/*
 * This file is part of GNU Taler, ©2022-25 Taler Systems S.A.
 * See LICENSE.md
 */
/**
 * @author Marc Stibane
 */
import SwiftUI

fileprivate let grouped = true

public struct WalletColors {
    @AppStorage("minimalistic") var minimalistic: Bool = false

//    let tint = Color(.tintColor)
    let gray1 = Color(.systemGray)          // uncompleted
    let gray2 = Color(.systemGray2)         // disabled Fore
    let gray3 = Color(.systemGray3)
    let gray4 = Color(.systemGray4)
    let gray5 = Color(.systemGray5)
    let gray6 = Color(.systemGray6)
    let gray7 = grouped ? Color(.tertiarySystemGroupedBackground)
                        : Color(.tertiarySystemBackground)        // enabled Back
    let gray8 = grouped ? Color(.secondarySystemGroupedBackground)
                        : Color(.secondarySystemBackground)       // disabled Back

    let attention = Color.red       // TODO: WalletColors().errorColor
    let confirm = Color.yellow      // TODO: WalletColors().warningColor

    // TODO: In Japan, negative is blue, and positive is red
    let positive = Color.green
    let negative = Color.red

    func buttonForeColor(pressed: Bool,
                        disabled: Bool,
                       prominent: Bool = false) -> Color {
            disabled ? gray2
        : !prominent ? talerColor
           : pressed ? gray6 : Color.white
    }

    func buttonBackColor(pressed: Bool,
                        disabled: Bool,
                       prominent: Bool = false) -> Color {
            disabled ? gray7
         : prominent ? talerColor
           : pressed ? gray4 : gray5
    }

    func secondary(_ scheme: ColorScheme, _ contrast: ColorSchemeContrast) -> Color {
        return contrast == .increased ? .primary            // WCAG AAA for any scheme
                       : minimalistic ? .secondary          // not enough contrast
                    : scheme == .dark ? .secondary          // WCAG AA for dark scheme
                                      : Color(.darkGray)    // WCAG AA for light scheme
    }

    var labelColor: Color {
        Color(UIColor.label)
    }

    var backgroundColor: Color {
        grouped ? Color(.systemGroupedBackground)
                : Color(.systemBackground)
    }

    var sideBackground: Color {
        gray6
    }

    func pickerSelected(_ scheme: ColorScheme, _ contrast: ColorSchemeContrast) -> Color {
        return contrast == .increased ? (scheme == .dark ? gray3 : gray4)
                                      : (scheme == .dark ? gray4 : gray5)
    }

    var pickerBackground: Color {
        gray7
    }

    var fieldForeground: Color {              // text color
        Color.primary
    }

    var fieldBackground: Color {
        gray8
    }

    var uncompletedColor: Color {
        // used in TransactionRowView
        gray1
    }

    var errorColor: Color {
        Color("Error")
    }

    var talerColor: Color {
        Color("Taler")
    }

    func pendingColor(_ incoming: Bool) -> Color {
        incoming ? Color("PendingIncoming")
                 : Color("PendingOutgoing")
    }

    func transactionColor(_ incoming: Bool) -> Color {
        incoming ? Color("Incoming")
                 : Color("Outgoing")
    }
}
